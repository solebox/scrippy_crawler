import urlparse

import scrapy
import re

from scrapy.http import request
from scrapy.utils.httpobj import urlparse_cached
from scrapy.utils.project import get_project_settings
from urllib3.util import parse_url
from urllib import urlencode
import weakref

from scrippy_crawler.items import Site


class LarvalCrawler(scrapy.Spider):
    """
        this spider hates unicode - its a baby it didnt learn hebrew yet don't judge it.
    """
    name = "larva"

    # allowed_domains = [""] # god damn it
    start_urls = ['http://www.google.co.il'] # default settings please ignore
    _urlparse_cache = {}# weakref.WeakKeyDictionary()

    custom_settings = {
        "USER_AGENT": 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534+ (KHTML, like Gecko) BingPreview/1.0b',
        "DEPTH_LIMIT": 1,
        "TELNETCONSOLE_PORT": '1337',
        # Obey robots.txt rules
        "ROBOTSTXT_OBEY": False,
        # "REDIRECT_ENABLED": False
    }

    def __init__(self, start_urls=None, *args, **kwargs):
        super(LarvalCrawler, self).__init__(**kwargs)
        self.domain_regex = '((([a-zA-Z]{1})|([a-zA-Z]{1}[a-zA-Z]{1})|([a-zA-Z]{1}[0-9]{1})|([0-9]{1}[a-zA-Z]{1})|([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}[a-zA-Z0-9]))\\.)([a-zA-Z]{2,6}|[a-zA-Z0-9-]{2,30}\\.[a-zA-Z]{2,3})$'
        site_file = (get_project_settings()['SITE_FILE'])
        self.base_host = None
        if start_urls is None:
            with open(site_file, "r") as sites_file:
                start_urls = sites_file.readlines()
                start_urls = [start_url.rstrip("\n") for start_url in start_urls]
        else:
            start_urls = start_urls.split(",")
        self.logger.debug("loaded start urls: {}".format(start_urls))
        self.start_urls = start_urls

    def parse(self, response):
        self.evil_chars = r"[^a-zA-Z0-9\.\/\?\=\:\%+_-]"
        if response.meta['depth'] == 0:
            self.base_host = self.urlparse_and_cache(response.request.url).host
            self.logger.debug("setting base host: {}".format(self.base_host))

        self.logger.debug("extracting urls from {}".format(response))

        print("DEPTH: {}".format(response.meta['depth']))
        site = Site()
        site['domain'] = self.base_host
        site['url'] = response.request.url
        site['text'] = ""
        for text in response.xpath('//*/text()').extract():
            site['text'] += text
        yield site  # collect those texts

        for url in response.xpath('//a/@href').extract():
            if url is not None:
                self.logger.debug("found url: {}".format(url))
                if re.findall(self.evil_chars, url):
                    self.logger.debug("bad url ignoring")# unicode in urls, some programmers dont read the rfc ;)
                    continue
                parsed_url = self.urlparse_and_cache(url)
                host = parsed_url.host
                path = parsed_url.path
                host_suffix = self.base_host.lstrip("wwww.")

                # print(str(parsed_url) + "host: " + str(host) + " path: " + str(path))
                if host is not None:
                    if str(host).endswith(host_suffix):  # fixme: assoming a world without cdns and per country cnames (see comment below)
                        self.logger.debug("yielding, host: {} matched!".format(host))
                        yield scrapy.Request(url, callback=self.parse)
                    else:
                        pass
                        # todo: could be google.com for google.com for google.co.il start_url
                else:
                    if path and re.match(r"^/", path):
                        if re.findall(self.evil_chars, path):
                            self.logger.debug("bad url , please encode: {}".format(url))  # bad url please urlencode
                        else:
                            self.logger.debug("yielding path: {} matched!".format(path))
                            url = response.urljoin(path)
                            yield scrapy.Request(url, callback=self.parse)
                            # else:
                            #     print(host+path+"no match")
                            # yield scrapy.Request(url, callback=self.crawl_it)

    @classmethod
    def urlparse_and_cache(cls, url):
        """Return urlparse.urlparse caching the result, where the argument can be a
        Request or Response object
        """
        if url not in cls._urlparse_cache:
            cls._urlparse_cache[url] = parse_url(url)
        return cls._urlparse_cache[url]


