import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from scrippy_crawler.items import Site


class SimpleSpider(scrapy.Spider):
    name = "simple"

    # allowed_domains = [""] # god damn it

    _urlparse_cache = {}# weakref.WeakKeyDictionary()

    custom_settings = {
        "USER_AGENT": 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534+ (KHTML, like Gecko) BingPreview/1.0b',
        "DEPTH_LIMIT": 3,
        "TELNETCONSOLE_PORT": '1337',
        # Obey robots.txt rules
        "ROBOTSTXT_OBEY": False,
        #"REDIRECT_ENABLED": False
    }

    def __init__(self, start_urls=None, *args, **kwargs):
        super(SimpleSpider, self).__init__(**kwargs)
        site_file = (get_project_settings()['SITE_FILE'])
        if start_urls is None:
            with open(site_file, "r") as sites_file:
                start_urls = sites_file.readlines()
                start_urls = [start_url.rstrip("\n") for start_url in start_urls]
        else:
            start_urls = start_urls.split(",")
        self.start_urls = start_urls
        self.logger.debug("start urls: {}".format( start_urls))

    def parse(self, response):
        for text in response.xpath('//*/text()').extract():
            yield {'url': response.url, 'text': text}

        for url in response.xpath('//a/@href').extract():
            if url:
                # print("got url: {}".format(url))
                yield scrapy.Request(url, callback=self.parse)



