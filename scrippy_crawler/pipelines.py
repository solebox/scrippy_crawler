#!/usr/bin/python
# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


from scrapy.exceptions import DropItem
import re


class ScrippyCrawlerPipeline(object):
    def process_item(self, item, spider):
        text = item['text']
        if not re.sub('[\r\n\t]', '', text):
            raise DropItem("this is just whitespace dude")

        return item
