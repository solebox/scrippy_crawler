#!/usr/bin/env python
import sys
import requests 

def query(search_term):
	results = requests.get("http://localhost:9200/sites/site/_search?q=text:{}&_source=url,domain".format(search_term))
	hits = results.json()['hits']
	if hits['total']:
		for hit in hits['hits']:
			print(hit['_source'])
	else:
		print("we didnt find anything, life is pain")

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print("usage: {} <search term>".format(sys.argv[0]))
		sys.exit(1)
	query(sys.argv[1])
