install:
1. pip install -r requirements.txt

usage:
1. fill sites.txt with the sites to be crawled 
2. make sure the spider is configured to the correct depth (custom settings override aswell)
3. ./elastic/bin/elasticsearch
4. run stage 1 for the initial crawl and populize the db automagically 
	4.1 can run using the scrapy command: "scrapy crawl larva"
	4.2 i might make a bash script but this looks easy enough 
5. run stage 2 and pass it the word you desire to find refs to 
	5.1 simply use ./query.py <search-term>
6. ???
7. profit


p.s - since you said no i descided to make this little project a tad more fun and did some bonus work , (this is not limited to a single word and will output domain plus exact url i crawled) 
plus its parallel and uses elasticsearch , always wanted to learn scrapy and elastic. thanks for that. :)
oh and this thing doesnt like bad encoding and unicode so dont try israelie sites, id stick to ascii for now , sorry for that.
